import pygame as pg
from spritesheet import Spritesheet

SPRITE_SIZE = 16
ZOOM = 3
TILE_SIZE = SPRITE_SIZE * ZOOM

class Patron(pg.sprite.Sprite):
    def __init__(self, chair):
        super().__init__()

        self.x = chair.x
        self.y = chair.y
        self.chair = chair
        self.food = None
        self.bubble = None
        self.fed = False

        sprite_pos = 28 * SPRITE_SIZE, 4 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos)
        self.image = pg.transform.scale(self.image,(TILE_SIZE, TILE_SIZE))
        self.rect = pg.Rect(self.x * TILE_SIZE, self.y * TILE_SIZE, TILE_SIZE, TILE_SIZE)

    def render(self):
        self.bubble = Spritesheet('bubble.png').image_at((0, 0, SPRITE_SIZE, SPRITE_SIZE), -1)
        rect = self.bubble.get_rect()
        if self.food:
            # Only add food if it's set
            food = pg.transform.scale(self.food.image, (int(SPRITE_SIZE * 0.5), int(SPRITE_SIZE * 0.5)))
            food_rect = food.get_rect()
            food_rect.center = rect.center
            self.bubble.blit(food, food_rect)
        self.bubble = pg.transform.scale(self.bubble, (int(SPRITE_SIZE * 2.5), int(SPRITE_SIZE * 2.5)))
        self.bubble_rect = self.bubble.get_rect()
        self.bubble_rect.center = self.rect.center
        self.bubble_rect.y -= 2 * TILE_SIZE / 3

    def update(self, dt):
        pass
    
    def draw(self, surf):
        if not self.bubble:
            self.render()
        surf.blit(self.image, self.rect)
        surf.blit(self.bubble, self.bubble_rect)