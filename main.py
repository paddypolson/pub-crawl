import pygame as pg
import sys

pg.init()

WINDOW_SIZE = (1248, 720)
TILE_SIZE = 16
ANIMATION_TIME = 200

SCREEN = pg.display.set_mode(WINDOW_SIZE, pg.FULLSCREEN)
pg.display.set_caption("Game Jam")
CLOCK = pg.time.Clock()

from player import *
from hub import *

HUB_SPAWN_POINT = (25, 7)
LEVEL_SPAWN_FROM_HUB = (0, 7)

class Game(object):
    
    def __init__(self, player, levels, hub):
        super().__init__()
        self.player = player
        self.levels = levels
        self.hub = hub

        self.player.game = self

        self.current_level = self.hub
        self.level_counter = 0

        for level in self.levels:
            level.set_steps()

    def move_hub(self):
        self.current_level = self.hub
        self.player.level = self.current_level
        self.player.set_position(HUB_SPAWN_POINT)
    
    def move_up_level(self):
        if self.current_level.is_hub():
            self.level_counter = 0
            self.player.set_position(LEVEL_SPAWN_FROM_HUB)
            self.current_level = self.levels[self.level_counter]
            self.player.level = self.current_level
            return
        elif self.level_counter < (len(self.levels) - 1):
            self.level_counter += 1
        else:
            # Can't!
            return False
        self.current_level = self.levels[self.level_counter]
        self.player.level = self.current_level
        self.player.set_position((self.player.x, 14))
    
    def move_down_level(self):
        if self.current_level.is_hub():
            # WUT?
            raise Exception
        elif self.level_counter <= 0:
            # Can't
            return False
        else:
            self.level_counter -= 1
        self.current_level = self.levels[self.level_counter]
        self.player.level = self.current_level
        self.player.set_position((self.player.x, 0))

    def update(self, dt):
        # Run the update tick
        self.player.update(dt)
        self.current_level.update(dt)

    def draw(self, surf):
        surf.fill('black')

        self.current_level.draw(surf)
        self.player.draw(surf)


def main():
    pg.key.set_repeat(ANIMATION_TIME)
    level_size = (26, 15)

    hub = Hub(level_size, "hub.txt")
    player = Player(8, 8, hub)

    level_files = ["one.txt", "two.txt", "three.txt"]
    levels = [Level(level_size, file_name) for file_name in level_files]

    game = Game(player, levels, hub)

    while True:
        dt = CLOCK.tick(60) / 1000

        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                quit(0)
            
            # if event.type == pg.MOUSEBUTTONDOWN:
            #     level.generate()
            #     level.render()


            if event.type == pg.KEYDOWN:

                if event.key == pg.K_a:
                    player.move_left()
                
                elif event.key == pg.K_d:
                    player.move_right()
                
                elif event.key == pg.K_w:
                    player.move_up()
                
                elif event.key == pg.K_s:
                    player.move_down()
                
                elif event.key == pg.K_SPACE:
                    player.grab()
                
                elif event.key == pg.K_r:
                    safe_spot = (2, 7)
                    player.set_position(safe_spot)
                    player.level.generate()
            
            if event.type == pg.KEYUP:

                if event.key == pg.K_SPACE:
                    player.release()

        game.update(dt)

        game.draw(SCREEN)

        pg.display.flip()

main()