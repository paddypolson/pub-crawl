from level import *

class Hub(Level):
    def __init__(self, size, file_name):
        super().__init__(size, file_name)
        self.step_counters = []

    def generate(self):

        self.tables = pg.sprite.Group()
        self.items = pg.sprite.Group()
        self.patrons = pg.sprite.Group()
        self.food_spawners = pg.sprite.Group()
        self.doors = pg.sprite.Group()

        level = [[None for x in range(self.width)] for y in range(self.height)]
        with open(self.file_name, 'r') as tavern:
            rows = tavern.readlines()

            for y in range(self.height):
                for x in range(self.width):
                    tile = rows[y][x]
                    if tile == '0':
                        level[y][x] = Ground(x, y)
                    if tile == '1':
                        level[y][x] = Wall(x, y)
                    if tile == '2':
                        level[y][x] = Fence(x, y)
                    if tile == '3':
                        level[y][x] = Door(x, y)
                    if tile == '4':
                        level[y][x] = Window(x, y)
                    if tile == '5':
                        level[y][x] = Bed(x, y)
                    if tile == '6':
                        level[y][x] = Shelf(x, y)
                    if tile == '8':
                        level[y][x] = Path(x, y)
                    if tile == '9':
                        level[y][x] = Grass(x, y)
                    if tile.isalpha():
                        level[y][x] = Floor(x, y)
                        self.items.add(Box(x, y, self, tile))
        self._level = level
    
    def is_hub(self):
        return True

    def update(self, dt):
        for item in self.items:
            item.update(dt)
