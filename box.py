import pygame as pg
from spritesheet import Spritesheet

SPRITE_SIZE = 16
ZOOM = 3
TILE_SIZE = SPRITE_SIZE * ZOOM

class Movable(pg.sprite.Sprite):
    def __init__(self, x, y, level):
        super().__init__()

        self.x = x
        self.y = y

        self.level = level

        sprite_pos = 28 * SPRITE_SIZE, 12 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.highlight = Spritesheet('highlight.png').image_at((0, 0, SPRITE_SIZE, SPRITE_SIZE), -1)
        self.highlight = pg.transform.scale( self.highlight, (TILE_SIZE, TILE_SIZE))

        self.selected = Spritesheet('selected.png').image_at((0, 0, SPRITE_SIZE, SPRITE_SIZE), -1)
        self.selected = pg.transform.scale( self.selected, (TILE_SIZE, TILE_SIZE))

        self.grabbed = False
        self.grabable = False
    
    def resize(self):
        self.image = pg.transform.scale( self.image, (TILE_SIZE, TILE_SIZE))
        self.rect = pg.Rect(self.x * TILE_SIZE, self.y * TILE_SIZE, TILE_SIZE, TILE_SIZE)
        
    def draw(self, surf):
        surf.blit(self.image, self.rect)
        if self.grabable:
            surf.blit(self.highlight, self.rect)
        if self.grabbed:
            surf.blit(self.selected, self.rect)
    
    def get_position(self):
        return self.x, self.y
    
    def is_coin(self):
        return False

    def check_move(self, dest):
        # Check if dest is walkable
        if dest.is_table():
            return True
        elif not dest.walkable:
            return False
        # Move is ok
        return True
    
    def push_right(self):
        dest = self.level.get_tile(self.x + 1, self.y)
        if not self.check_move(dest):
            return False # Hit a wall
        for item in self.level.items:
            if item.x == dest.x and item.y == dest.y:
                # Something in the way
                if item.push_right(): # Try push the colliding item
                    # All good
                    pass
                else:
                    return False
        self.x += 1
        self.rect.move_ip(TILE_SIZE, 0)
        return True # Everything worked out
    
    def push_left(self):
        dest = self.level.get_tile(self.x - 1, self.y)
        if not self.check_move(dest):
            return False # Hit a wall
        for item in self.level.items:
            if item.x == dest.x and item.y == dest.y:
                # Something in the way
                if item.push_left(): # Try push the colliding item
                    # All good
                    pass
                else:
                    return False
        self.x += -1
        self.rect.move_ip(-TILE_SIZE, 0)
        return True # Everything worked out
    
    def push_up(self):
        dest = self.level.get_tile(self.x, self.y - 1)
        if not self.check_move(dest):
            return False # Hit a wall
        for item in self.level.items:
            if item.x == dest.x and item.y == dest.y:
                # Something in the way
                if item.push_up(): # Try push the colliding item
                    # All good
                    pass
                else:
                    return False
        self.y -= 1
        self.rect.move_ip(0, -TILE_SIZE)
        return True # Everything worked out
    
    def push_down(self):
        dest = self.level.get_tile(self.x, self.y + 1)
        if not self.check_move(dest):
            return False # Hit a wall
        for item in self.level.items:
            if item.x == dest.x and item.y == dest.y:
                # Something in the way
                if item.push_down(): # Try push the colliding item
                    # All good
                    pass
                else:
                    return False
        self.y += 1
        self.rect.move_ip(0, TILE_SIZE)
        return True # Everything worked out

class Coin(Movable):
    def __init__(self, x, y, level):
        super().__init__(x, y, level)
        sprite_pos = 41 * SPRITE_SIZE, 3 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos, -1)
        self.resize()
    
    def is_coin(self):
        return True

class Chest(Movable):
    def __init__(self, x, y, level):
        super().__init__(x, y, level)
        sprite_pos = 10 * SPRITE_SIZE, 6 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos, -1)
        self.resize()

class Cauldron(Movable):
    def __init__(self, x, y, level):
        super().__init__(x, y, level)
        sprite_pos = 5 * SPRITE_SIZE, 14 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos, -1)
        self.resize()

class Bath(Movable):
    def __init__(self, x, y, level):
        super().__init__(x, y, level)
        sprite_pos = 4 * SPRITE_SIZE, 14 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos, -1)
        self.resize()

class Torch(Movable):
    def __init__(self, x, y, level):
        super().__init__(x, y, level)
        sprite_pos = 4 * SPRITE_SIZE, 15 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos, -1)
        self.resize()

class Box(Movable):
    def __init__(self, x, y, level, letter=None):
        super().__init__(x, y, level)

        sprite_pos = 39 * SPRITE_SIZE, 14 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos)

        if letter:
            x, y = 0, 0
            if letter.isalpha():
                num = ord(letter) - ord('a')
                y = 18 # Start of y

                if num >= 13:# On the second row of letters
                    num -= 13
                    y = 19
                x = num + 35

            else:
                y = 17 # Start of y
                x = int(letter) + 35

            sprite_pos = x * SPRITE_SIZE, y * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
            letter = Spritesheet('colored_packed.png').image_at(sprite_pos, -1)
            self.image.blit(letter, letter.get_rect())
        
        self.resize()

    