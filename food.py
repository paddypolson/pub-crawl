from player import *
from box import *

SPRITE_SIZE = 16
ZOOM = 2
TILE_SIZE = SPRITE_SIZE * ZOOM

class Food(Movable):

    def __init__(self, x, y, level):
        super().__init__(x, y, level)
    
    def get_position(self):
        return self.x, self.y

    def update(self, dt):
        pass
    

class Fish(Food):
    def __init__(self, x, y, level):
        super().__init__(x, y, level)
        sprite_pos = 33 * SPRITE_SIZE, 17 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos, -1)
        self.resize()

class Bread(Food):
    def __init__(self, x, y, level):
        super().__init__(x, y, level)
        sprite_pos = 47 * SPRITE_SIZE, 7 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos, -1)
        self.resize()

class Meat(Food):
    def __init__(self, x, y, level):
        super().__init__(x, y, level)
        sprite_pos = 33 * SPRITE_SIZE, 16 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos, -1)
        self.resize()

class Cheese(Food):
    def __init__(self, x, y, level):
        super().__init__(x, y, level)
        sprite_pos = 34 * SPRITE_SIZE, 16 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(sprite_pos, -1)
        self.resize()