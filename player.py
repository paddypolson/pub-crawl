import pygame as pg
from spritesheet import Spritesheet
from level import Level

SPRITE_SIZE = 16
ZOOM = 3
TILE_SIZE = SPRITE_SIZE * ZOOM

class Player(pg.sprite.Sprite):
    def __init__(self, x, y, level: Level):
        super().__init__()

        self.x = x
        self.y = y

        player_sprite_pos = 30 * SPRITE_SIZE, 3 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE
        self.image = Spritesheet('colored_packed.png').image_at(player_sprite_pos, -1)
        self.image = pg.transform.scale(
            self.image,
            (
                TILE_SIZE,
                TILE_SIZE
            )
        )
        self.rect = pg.Rect(self.x * TILE_SIZE, self.y * TILE_SIZE, TILE_SIZE, TILE_SIZE)
        self.level = level

        self.grabbed = []
        self.grabable = []

        self.game = None
    
    def set_position(self, pos):
        self.x, self.y = pos
        self.rect = pg.Rect(self.x * TILE_SIZE, self.y * TILE_SIZE, TILE_SIZE, TILE_SIZE)
        if self.in_step_range() and not self.level.done:
            self.level.inc_steps()

    def update(self, dt):
        for item in self.grabable:
            item.grabable = False
        self.grabable = []
        neighbours = self.level.get_neighbours_von_neumann(self.x, self.y)
        positions = [n.get_position() for n in neighbours]

        for item in self.level.items:
            if item.get_position() in positions:
                self.grabable.append(item)
                item.grabable = True
    
    def draw(self, surf):
        surf.blit(self.image, self.rect)
    
    def check_move(self, dest):
        # Check if dest is walkable
        if not dest.walkable:
            return False
        # Move is ok
        return True
    
    def in_step_range(self):
        return self.level.step_range.collidepoint(self.x, self.y)
    
    def check_item(self, dest):
        for item in self.level.items:
            if item.is_coin():
                if item.x == dest.x and item.y == dest.y:
                    item.kill()
            if item.x == dest.x and item.y == dest.y:
                # Food in the way
                return item
        return False
    
    def grab(self):
        if self.grabbed:
            return
        neighbours = self.level.get_neighbours_von_neumann(self.x, self.y)
        positions = [n.get_position() for n in neighbours]

        for item in self.level.items:
            if item.get_position() in positions:
                self.grabbed.append(item)
                item.grabbed = True
    
    def release(self):
        for item in self.grabbed:
            item.grabbed = False
        self.grabbed = []
    
    def move_right(self):
        dest_pos = (self.x + 1, self.y)
        try:
            dest = self.level.get_tile(*dest_pos)
        except IndexError:
            # Moving off screen
            if self.level.is_hub():
                self.game.move_up_level()
            return False
        if not self.check_move(dest):
            return False
        
        # Check for food to move
        item = self.check_item(dest)
        if item and (item not in self.grabbed):
            if not item.push_right():
                return False
        
        # Check and move grabbed
        for item in self.grabbed:
            dest = self.level.get_tile(item.x + 1, item.y)
            if not item.check_move(dest):
                return False # Hit a wall
        
        for item in self.grabbed:
            if not item.push_right():
                return False
        
        self.set_position(dest_pos)
    
    def move_left(self):
        dest_pos = (self.x - 1, self.y)
        try:
            dest = self.level.get_tile(*dest_pos)
        except IndexError:
            # Moving off screen
            if self.level.is_hub():
                pg.quit()
                exit(0)
            else:
                self.game.move_hub()
                return False
        
        if not self.check_move(dest):
            return False

        # Check for food to move
        item = self.check_item(dest)
        if item and (item not in self.grabbed):
            if not item.push_left():
                return False
        
        # Check and move grabbed
        for item in self.grabbed:
            dest = self.level.get_tile(item.x - 1, item.y)
            if not item.check_move(dest):
                return False # Hit a wall
        for item in self.grabbed:
            if not item.push_left():
                return False

        self.set_position(dest_pos)
    
    def move_up(self):
        dest_pos = (self.x, self.y - 1)
        try:
            dest = self.level.get_tile(*dest_pos)
        except IndexError:
            self.game.move_up_level()
            return False
        
        if not self.check_move(dest):
            return False

        # Check for food to move
        item = self.check_item(dest)
        if item and (item not in self.grabbed):
            if not item.push_up():
                return False
        
        # Check and move grabbed
        for item in self.grabbed:
            dest = self.level.get_tile(item.x, item.y - 1)
            if not item.check_move(dest):
                return False # Hit a wall
        for item in self.grabbed:
            if not item.push_up():
                return False
        
        self.set_position(dest_pos)
    
    def move_down(self):
        dest_pos = (self.x, self.y + 1)
        try:
            dest = self.level.get_tile(*dest_pos)
        except IndexError:
            self.game.move_down_level()
            return False

        if not self.check_move(dest):
            return False

        # Check for food to move
        item = self.check_item(dest)
        if item and (item not in self.grabbed):
            if not item.push_down():
                return False
        
        # Check and move grabbed
        for item in self.grabbed:
            dest = self.level.get_tile(item.x, item.y + 1)
            if not item.check_move(dest):
                return False # Hit a wall
        for item in self.grabbed:
            if not item.push_down():
                return False
        
        self.set_position(dest_pos)

