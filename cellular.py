    def generate(self):

        seed(0)

        # Use cellular automata to generate map
        chanceToStartAlive = 0.5
        maxlimit = 4
        minlimit = 4
        numberOfSteps = 3

        def gen_step():
            new_level = [[False for x in range(self.width)] for y in range(self.height)]
            
            for y in range(self.height):
                for x in range(self.width):
                    count = 0
                    neighbours = self.get_neighbours(x, y)
                    count += abs(len(neighbours) - 8) # Make out of bounds wall
                    for neighbour in neighbours:
                        if neighbour:
                            count += 1
                    
                    if self.get_tile(x, y):
                        if count < minlimit:
                            new_level[y][x] = False
                        else:
                            new_level[y][x] = True
                    
                    else:
                        if count > maxlimit:
                            new_level[y][x] = True
            
            self._level = new_level

        # Start by randomy populating the level
        for y in range(self.height):
            row = []
            for x in range(self.width):
                if random() < chanceToStartAlive:
                    row.append(True)
                else:
                    row.append(False)
            self._level.append(row)
        
        # Iterate a number of times
        for i in range(numberOfSteps):
            gen_step()
        
        # Convert tiles back to tile objects
        new_level = [[None for x in range(self.width)] for y in range(self.height)]
        for y in range(self.height):
            for x in range(self.width):
                if self.get_tile(x, y):
                    new_level[y][x] = Wall(x, y)
                else:
                    new_level[y][x] = Floor(x, y)
        self._level = new_level