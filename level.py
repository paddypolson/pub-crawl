import pygame as pg
from spritesheet import Spritesheet
from random import random, seed, choice
from box import Box, Torch, Chest, Cauldron, Bath, Coin
import food
import patron

SPRITE_SIZE = 16
ZOOM = 3
spritesheet = Spritesheet("colored_packed.png")

class Tile(pg.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.x = x
        self.y = y
        self.rect = pg.Rect(x * SPRITE_SIZE, y * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE)
        self.walkable = True

    def get_position(self):
        return self.x, self.y
    
    def is_table(self):
        return False

    def draw(self, surf):
        surf.blit(self.image, self.rect)
    
    def update(self, dt):
        pass

class Door(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((6 * SPRITE_SIZE, 9 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))

class LockedDoor(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((3 * SPRITE_SIZE, 9 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        self.walkable = False

class Light(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((3 * SPRITE_SIZE, 15 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        self.walkable = False

class Chair(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((1 * SPRITE_SIZE, 8 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        self.table = None
        self.walkable = False

class Table(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((2 * SPRITE_SIZE, 8 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        self.walkable = False
        self.chair = None
        self.patron = None
    
    def is_table(self):
        return True

class FoodSpawner(Table):
    def __init__(self, x, y, food_type):
        super().__init__(x, y)
        self.food_type = food_type
        self.food = None
    
    def spawn(self, level):
        self.food = self.food_type(self.x, self.y, level)

class Sign(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((14 * SPRITE_SIZE, 7 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))

class Window(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((2 * SPRITE_SIZE, 13 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        self.walkable = False

class Tree(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((2 * SPRITE_SIZE, 1 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        self.walkable = False

class Wall(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((0, 13 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        self.walkable = False

class Fence(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((5 * SPRITE_SIZE, 3 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        self.walkable = False

class Bed(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((5 * SPRITE_SIZE, 8 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        self.walkable = False

class Shelf(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        r = random()
        if r < 0.2:
            self.image = spritesheet.image_at((3 * SPRITE_SIZE, 7 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        elif r < 0.4:
            self.image = spritesheet.image_at((4 * SPRITE_SIZE, 7 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        elif r < 0.6:
            self.image = spritesheet.image_at((5 * SPRITE_SIZE, 7 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        elif r < 0.8:
            self.image = spritesheet.image_at((7 * SPRITE_SIZE, 7 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        else:
            self.image = spritesheet.image_at((0 * SPRITE_SIZE, 8 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        self.walkable = False

class Ground(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        if random() < 0.1:
            self.image = spritesheet.image_at((1 * SPRITE_SIZE, 0 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        else:
            self.image = spritesheet.image_at((0 * SPRITE_SIZE, 0 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))

class Path(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((2 * SPRITE_SIZE, 0 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))

class Grass(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        r = random()
        if r < 0.33:
            self.image = spritesheet.image_at((5 * SPRITE_SIZE, 0 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        elif r < 0.67:
            self.image = spritesheet.image_at((6 * SPRITE_SIZE, 0 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))
        else:
            self.image = spritesheet.image_at((7 * SPRITE_SIZE, 0 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE))

class Floor(Tile):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.image = spritesheet.image_at((2 * SPRITE_SIZE, 0, SPRITE_SIZE, SPRITE_SIZE))

class Level(object):

    def __init__(self, size, file_name):
        super().__init__()

        self.size = pg.Vector2(*size)
        self.width, self.height = size
        self._level = []
        self.rect = pg.Rect((0, 0), (self.size.x * SPRITE_SIZE, self.size.y * SPRITE_SIZE))

        self.file_name = file_name

        self.tables = pg.sprite.Group()
        self.items = pg.sprite.Group()
        self.patrons = pg.sprite.Group()
        self.food_spawners = pg.sprite.Group()
        self.doors = pg.sprite.Group()
        self.coins = pg.sprite.Group()

        self.done = False
        self.steps = 0
        self.step_counters = [(16, 12), (17, 12), (18, 12), (19, 12), (20, 12)]
        self.step_range = pg.Rect(5, 2, 22, 13)

        self.image = None
        self.generate()
    
    def set_steps(self):
        steps = f'{self.steps:05}'
        for counter, c in zip(self.step_counters, steps):
            self.set_item(*counter, Box(*counter, self, c))

    def inc_steps(self):
        self.steps += 1
        self.set_steps()

    def is_hub(self):
        return False
    
    def set_item(self, x, y, item):
        # remove old item
        old_item = self.get_item(x, y)
        if old_item: 
            old_item.kill()
        self.items.add(item)
    
    def get_item(self, x, y):
        if x < 0 or y < 0:
            raise IndexError
        if x >= self.size.x or y >= self.size.y:
            raise IndexError
        for item in self.items:
            if x == item.x and y == item.y:
                return item

    def get_tile(self, x, y):
        if x < 0 or y < 0:
            raise IndexError
        if x >= self.size.x or y >= self.size.y:
            raise IndexError
        return self._level[y][x]
    
    def get_neighbours_von_neumann(self, x, y):
        tiles = []
        if x > 0:
            try:
                tiles.append(self.get_tile(x - 1, y))
            except IndexError:
                pass
        if x < self.width:
            try:
                tiles.append(self.get_tile(x + 1, y))
            except IndexError:
                pass
        if y > 0:
            try:
                tiles.append(self.get_tile(x, y - 1))
            except IndexError:
                pass
        if y < self.width:
            try:
                tiles.append(self.get_tile(x, y + 1))
            except IndexError:
                pass
        return tiles
    
    def get_neighbours_moore(self, x, y):
        tiles = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                neighbour_x = x + j
                neighbour_y = y + i

                if (i == 0 and j == 0):
                    # This tile, skip
                    continue
                
                elif (neighbour_x < 0 or neighbour_y < 0 or neighbour_x >= self.width or neighbour_y >= self.height):
                    # Tile is out of bounds
                    continue

                # Passed checks, add tile to neighbours
                tiles.append(self.get_tile(neighbour_x, neighbour_y))
        
        return tiles

    def generate(self):

        self.tables = pg.sprite.Group()
        self.items = pg.sprite.Group()
        self.patrons = pg.sprite.Group()
        self.food_spawners = pg.sprite.Group()
        self.doors = pg.sprite.Group()
        self.steps = 0

        food_types = [food.Bread, food.Cheese, food.Fish, food.Meat]
        level = [[None for x in range(self.width)] for y in range(self.height)]
        with open(self.file_name, 'r') as tavern:
            rows = tavern.readlines()

            for y in range(self.height):
                for x in range(self.width):
                    tile = rows[y][x]
                    if tile == 'F':
                        tile = Floor(x, y)
                    elif tile == 'B':
                        tile = Bed(x, y)
                    elif tile == 'L':
                        tile = Ground(x, y)
                        self.items.add(Torch(x, y, self))
                    elif tile == '+':
                        tile = Ground(x, y)
                        self.items.add(choice([Bath, Cauldron, Chest])(x, y, self))
                    elif tile == '*':
                        tile = Ground(x, y)
                        coin = Coin(x, y, self)
                        self.items.add(coin)
                        self.coins.add(coin)
                    elif tile == 'W':
                        tile = Wall(x, y)
                    elif tile == 'D':
                        tile = Door(x, y)
                    elif tile == 'S':
                        tile = LockedDoor(x, y)
                        self.doors.add(tile)
                    elif tile == 'Q':
                        tile = Fence(x, y)
                    elif tile == 'G':
                        tile = Ground(x, y)
                    elif tile == 'O':
                        tile = Window(x, y)
                    elif tile == 'T':
                        tile = Table(x, y)
                        self.tables.add(tile)
                    elif tile == 'C':
                        tile = Chair(x, y)
                    elif tile == 'P':
                        tile = FoodSpawner(x, y, food_types.pop())
                        self.food_spawners.add(tile)
                    elif tile.islower():
                        self.items.add(Box(x, y, self, tile))
                        tile = Ground(x, y)
                    
                    level[y][x] = tile

        self._level = level

        # Set the chair for each table
        for table in self.tables:
            neighbours = self.get_neighbours_von_neumann(table.x, table.y)
            for n in neighbours:
                if type(n) == Chair:
                    table.chair = n
                    n.table = table
                    self.new_patron(n, table) # Add a patron to the chair

        # Finally re-render()
        self.render()

    def new_patron(self, chair, table):
        new_patron = patron.Patron(chair)
        table.patron = new_patron
        self.patrons.add(new_patron)
        self.spawn_food(new_patron)

    def spawn_food(self, patron):
        for spawner in self.food_spawners:
            if spawner.food == None:
                spawner.spawn(self)
                patron.food = spawner.food
                self.items.add(spawner.food)
                return True
        return False # no tables to spawn food on
    
    def update(self, dt):
        fed = [patron.fed for patron in self.patrons]
        if False not in fed:
            for door in self.doors:
                x, y = door.get_position()
                self._level[y][x] = Door(x, y)
                self.render()
        
        if not self.coins and False not in fed:
            self.done = True

        for item in self.items:
            item.update(dt)
            for table in self.tables:
                if item.x == table.x and item.y == table.y:
                    if not table.patron:
                        pass
                    # Food is on the table!
                    elif item == table.patron.food:
                        item.kill()
                        table.patron.fed = True

    
    def render(self):
        self.image = pg.Surface((self.rect.width, self.rect.height)).convert()
        for row in self._level:
            for tile in row:
                self.image.blit(tile.image, tile.rect)

        # Resize to ZOOM level
        self.image = pg.transform.scale(
            self.image,
            (
                self.width * ZOOM * SPRITE_SIZE,
                self.height * ZOOM * SPRITE_SIZE
            )
        )


    def draw(self, surf):
        if not self.image:
            self.render()
        surf.blit(self.image, self.rect)
        for item in self.items:
            item.draw(surf)
        for item in self.patrons:
            item.draw(surf)